﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Type of the emnemy
/// int value denotes the damage that an enemy can do
/// enum requires unique value
/// </summary>
public enum enemy_Type{Zombie1 = 10, Zombie2 = 15, Zombie3 = 20, Zombie4 = 25, Bug1 = 8, Bug2 = 12}

public class EnemyCommonProperties : MonoBehaviour {

	#region Script's Summary
	//This script is controlling the behaviour which is common in all the enemies
	#endregion


	#region Variable Declaration
	//--------------------Variable Declaration-----------------------------

	[SerializeField] private NavMeshAgent self_Navmesh; 	//nav mesh agent of itself to navigate player
	[SerializeField] private Transform target;				//player position to follow
	[SerializeField] private GameObject self_Ragdoll;		//ragdoll of the zombie

	[SerializeField] private int self_health;				//health of the enemy
	[SerializeField] private enemy_Type self_Type;			//type of the enemy

	[SerializeField] private float damage_Time;				//after how many seconds the enemy can cause damage
	private float current_Time;								//will be used as a counter

	private Animator self_Anime;							//animator of itself

	private bool can_Not_Attack;							//enemy can not attack

	//----------------------------------------------------------------------
	#endregion

	#region Unity's method
	//----------------------Unity's Method----------------------------------

	void Awake (){

	}

	void Start () {

		//--------- variable definitions ---------

		self_Navmesh = GetComponent<NavMeshAgent> ();
		target = GameObject.FindGameObjectWithTag ("Player").transform;
		self_Anime = gameObject.GetComponent<Animator> ();

		self_health = 50;
		can_Not_Attack = false;

		InvokeRepeating ("RefreshTargetPosition", 1f, 1f); //refresh target position after every 1 seconds

		self_Anime.SetTrigger ("Run"); //trigger the run animation

		//-------------------------------------------
	}

	void Update () {

		if (Vector3.Distance (transform.position, target.position) < 6f) {
			AttackTarget ();
		} else if (can_Not_Attack) {
			RunTowardsTarget ();
		}
	}

	//----------------------------------------------------------------------
	#endregion

	#region User's Functions
	//----------------------User's Functions--------------------------------

	/// <summary>
	/// Refresh the target position for new paths in a predetermined no. of seconds.
	/// </summary>
	void RefreshTargetPosition(){

		self_Navmesh.SetDestination (target.transform.position); //refresh the position of the target for new routes
	}

	/// <summary>
	/// enemy idle state
	/// </summary>
	void IdleEnemy(){

		self_Anime.SetTrigger ("Idle"); //trigger the idle animation
	}

	/// <summary>
	/// Attacks the target.
	/// </summary>
	void AttackTarget(){


		if (current_Time >= damage_Time) {
			target.GetComponent<Player> ().HealthCount (-((int)self_Type));
			current_Time = 0;
		} else {
			current_Time += Time.deltaTime;
		}
		if(!can_Not_Attack)
			self_Anime.SetTrigger ("Attack"); //trigger the attack animation
		
		can_Not_Attack = true;

	}

	/// <summary>
	/// Runs towards the target.
	/// </summary>
	void RunTowardsTarget(){

		self_Anime.SetTrigger ("Run"); //trigger the run animation
		can_Not_Attack = false;
	}

	/// <summary>
	/// Kill the character.
	/// if enemy is a Bug then flag will be true 
	/// </summary>
	/// <param name="flag">true in case of bug</param>
	/// <param name="damage">Damage caused.</param>
	public void CharacterDie(bool flag, int damage){

		self_health -= damage; //damage to the enemy

		//if enemy's health is left then return
		if (self_health > 0)
			return;

		self_Navmesh.Stop(); //stopping enemy

		if (flag){
			self_Anime.SetTrigger ("Die"); //trigger the die animation
			Destroy(gameObject, 5f); //destroy the bugs after 5 seconds
		}else {
			Destroy (gameObject); //destroy the zombies immediately
			GameObject temp = Instantiate (self_Ragdoll, transform.position, transform.rotation) as GameObject; //generate the ragdoll
			Destroy(temp, 5f); //destroy ragdoll after 5 seconds

		}
	}

	//----------------------------------------------------------------------

	#endregion
}
