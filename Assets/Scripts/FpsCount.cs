﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FpsCount : MonoBehaviour {

	#region Script's Summary
	//This script is controlling the camera movement and raycasting to check for zombies
	#endregion


	#region Variable Declaration
	//--------------------Variable Declaration-----------------------------

	private float m_DeltaTime;   					// This is the smoothed out time between frames.
	[SerializeField] private Text m_Text;           // Reference to the component that displays the fps.
	private const float k_SmoothingCoef = 0.1f;     // This is used to smooth out the displayed fps.
	
	//----------------------------------------------------------------------
	#endregion

	#region Unity's method
	//----------------------Unity's Method----------------------------------

	void Awake (){

	}

	void Start () {

		//--------- variable definitions ---------


		//-------------------------------------------
	}

	void Update () {

		//mouse look
		if ((Mathf.Abs(Input.GetAxis ("Mouse X"))) > (Mathf.Abs(Input.GetAxis ("Mouse Y")))) {
			transform.Rotate (0, Mathf.Clamp((Input.GetAxis ("Mouse X")), -45f, 45f), 0);
		} else if ((Mathf.Abs(Input.GetAxis ("Mouse X"))) < (Mathf.Abs(Input.GetAxis ("Mouse Y")))) {
			transform.Rotate (-Input.GetAxis ("Mouse Y"), 0, 0);
		}

		UpdateFPS ();
	}

	//----------------------------------------------------------------------
	#endregion

	#region User's Functions
	//----------------------User's Functions--------------------------------

	/// <summary>
	/// VR FPS counter script
	/// </summary>
	void FPSCounter(){

		// This line has the effect of smoothing out delta time.
		m_DeltaTime += (Time.deltaTime - m_DeltaTime) * k_SmoothingCoef;

		// The frames per second is the number of frames this frame (one) divided by the time for this frame (delta time).
		float fps = 1.0f / m_DeltaTime;

		// Set the displayed value of the fps to be an integer.
		//m_Text.text = Mathf.FloorToInt (fps) + " fps";
	}


	#region Alt fps
	float updateInterval = 0.5f;
	float accum          = 0.0f;
	int   frames         = 0;
	float timeLeft       = 0.0f;
	string strFPS = null; // "FPS: 0";

	void UpdateFPS()
	{
		timeLeft -= Time.unscaledDeltaTime;
		accum += Time.unscaledDeltaTime;
		++frames;

		// Interval ended - update GUI text and start new interval
		if (timeLeft <= 0.0)
		{
			// display two fractional digits (f2 format)
			float fps = frames / accum;

			strFPS = System.String.Format("FPS: {0:F2}", fps);

			timeLeft += updateInterval;
			accum = 0.0f;
			frames = 0;
		}

		m_Text.text = strFPS; //fps at the center of the screen
	}

	#endregion

	//----------------------------------------------------------------------

	#endregion
}
