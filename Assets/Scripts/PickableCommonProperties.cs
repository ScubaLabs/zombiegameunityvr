﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRStandardAssets.Utils;

/// <summary>
/// type of the pickable
/// int value denotes the value of pickup
/// enum requires unique value
/// </summary>
public enum pickable_Type{Ammo = 20, Health = 50};

public class PickableCommonProperties : MonoBehaviour {

	#region Script's Summary
	//This script is responsible for managing the common behaviour of the pickables
	#endregion


	#region Variable Declaration
	//--------------------Variable Declaration-----------------------------

	[SerializeField] private VRInteractiveItem vr_Interactable; //make this item VR interactive
	[SerializeField] private SelectionRadial vr_Radial; 		//VR selection radial

	[SerializeField] private float rotation_Speed;				//rotation speed of the pickable

	[SerializeField] private pickable_Type self_Type; 			//type of the pickable

	public pickable_Type get_Pickable_Type{
		get{ return self_Type; }
	}

	//----------------------------------------------------------------------
	#endregion

	#region Unity's method
	//----------------------Unity's Method----------------------------------

	void Awake (){

	}

	void Start () {

		//--------- variable definitions ---------

		rotation_Speed = 2f;

		//-------------------------------------------
	}

	void Update () {

		//rotate the pickable item
		if (self_Type == pickable_Type.Health){
			transform.Rotate (Vector3.forward * rotation_Speed); // in the case of first aid kit
		}else {
			transform.Rotate (Vector3.up * rotation_Speed); 
		}
	}

	//----------------------------------------------------------------------
	#endregion

	#region User's Functions
	//----------------------User's Functions--------------------------------

	//----------------------------------------------------------------------

	#endregion
}
