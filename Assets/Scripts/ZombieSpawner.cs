﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieSpawner : MonoBehaviour {

	#region Script's Summary
	//This script is spawning zombies randomly on predetermined points
	#endregion


	#region Variable Declaration
	//--------------------Variable Declaration-----------------------------

	[SerializeField] private GameObject spawn_Point_Parent; //parent of all the spawn points
	[SerializeField] private int max_zombie_Count;			//max. number of zombies allowed in the game
	[SerializeField] private int max_Bug_Count;				//max. number of bugs allowed in the game

	[SerializeField] private GameObject[] zombie_Prefab;	//all the zombies that need to be spawn
	[SerializeField] private GameObject[] bug_Prefab;		//all the bugs that need to be spawn

	private Transform[] spawn_Points; 						//array of spawn points derived from the parent
	private int zombie_Count; 								//total zombies spawned
	private int bug_Count;									//total zombies spawned

	//----------------------------------------------------------------------
	#endregion

	#region Unity's method
	//----------------------Unity's Method----------------------------------

	void Awake (){

	}

	void Start () {

		//--------- variable definitions ---------

		zombie_Count = 0; //total spawned zombies

		spawn_Points = spawn_Point_Parent.GetComponentsInChildren<Transform> (); //get all the spawn points(children) 
		//Debug.Log (spawn_Points.Length);

		//-------------------------------------------
	}

	void Update () {

		//if total zombies spawned is less than max zombie that can spawn
		if (zombie_Count < max_zombie_Count) {
			SpawnEnemy ("zombie");
		}
		if (bug_Count < max_Bug_Count) {
			SpawnEnemy ("bug");
		}
	}

	//----------------------------------------------------------------------
	#endregion

	#region User's Functions
	//----------------------User's Functions--------------------------------

	/// <summary>
	/// Gets or sets the zombie_Count.
	/// </summary>
	/// <value>The counter.</value>
	public int counter{
		get{return zombie_Count;}
		set{zombie_Count = value;}
	}

	/// <summary>
	/// Spawns a zombie on a random position.
	/// </summary>
	private void SpawnEnemy(string type){

		GameObject temp_Obj;
		GameObject temp_Prefab;
		int enemy_Index;

		int spawn_Index = Random.Range (1, spawn_Points.Length);

		if (type == "zombie") {
			enemy_Index = Random.Range (0, zombie_Prefab.Length);
			temp_Prefab = zombie_Prefab[enemy_Index];
			zombie_Count++;  //increase the total zombie spawn count
		} else {
			enemy_Index = Random.Range (0, bug_Prefab.Length);
			temp_Prefab = bug_Prefab[enemy_Index];
			bug_Count++;
		}

		//spawn zombie and set its position to spawn_Points [index].transform.position
		//set its parent to spawn_Points [index].transform

		temp_Obj = Instantiate (temp_Prefab, spawn_Points [spawn_Index].transform.position, Quaternion.identity) as GameObject;
		temp_Obj.transform.SetParent (spawn_Points [spawn_Index].transform);

	}
	//----------------------------------------------------------------------

	#endregion
}
