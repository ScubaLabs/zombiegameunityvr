﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

	#region Script's Summary
	//This script is controlling the player
	#endregion


	#region Variable Declaration
	//--------------------Variable Declaration-----------------------------

	[Tooltip("drag the 'Eyes' from the left hierarchy menu")]
	[SerializeField] private GameObject self_Eye;			//Eyes(main camera) of player

	[SerializeField] private float walk_Speed;				//walking speed of the player
	[SerializeField] private float turn_Speed;				//turning speed of the player
	[SerializeField] private int player_HP;					//HP of the player

	private CameraControl vr_Input_Script;					//self made vr script
	private int player_Health; 								//current health of the player
	private int player_Ammo;								//current ammo of the player
	private Animator player_Anime;							//player's self animator

	private float temp_Speed;

	[SerializeField] private Text temp_Text;

	//----------------------------------------------------------------------
	#endregion

	#region Unity's method
	//----------------------Unity's Method----------------------------------

	void Awake (){

	}

	void Start () {

		//--------- variable definitions ---------

		vr_Input_Script = self_Eye.GetComponent<CameraControl> ();

		player_Anime = GetComponent<Animator> ();

		player_Health = 500; //initial health of the player
		player_Ammo = 50;	 //initial ammo of the player
		player_HP = 20;		 //initial HP of the player

		//-------------------------------------------
	}

	void Update () {
		
		Movement ();

		if (vr_Input_Script.touch_Status == touch_Stages.click)
			Shoot ();
	}

	/// <summary>
	/// only pickables have triggers
	/// </summary>
	/// <param name="target">Target.</param>
	void OnTriggerEnter(Collider target){

		if (!target.CompareTag ("Pickable"))
			return;
		
		if (target.GetComponent<PickableCommonProperties> ().get_Pickable_Type == pickable_Type.Health) {
			HealthCount ((int)pickable_Type.Health);
		}else if(target.GetComponent<PickableCommonProperties>().get_Pickable_Type == pickable_Type.Ammo){
			AmmoCount ((int)pickable_Type.Ammo);
		}

		Destroy(target.transform.parent.gameObject);	//destroy the pickable
	}
	//----------------------------------------------------------------------
	#endregion

	#region User's Functions
	//----------------------User's Functions--------------------------------

	/// <summary>
	/// Movement of the player
	/// </summary>
	void Movement(){ 

		if (vr_Input_Script.swipe_Status == swipe_Speed.none) {
			temp_Text.text = "";
			return;
		}
		
		//camera movement
		if (vr_Input_Script.touch_Status == touch_Stages.swipe_Left) {
			transform.Rotate (0, -turn_Speed * Time.deltaTime, 0);
			return;
		}else if (vr_Input_Script.touch_Status == touch_Stages.swipe_Right) {
			transform.Rotate (0, turn_Speed * Time.deltaTime, 0);
			return;
		}
		//Quaternion temp;
		//temp = self_Eye.transform.rotation;
		//transform.Rotate (0, -temp.y, 0);
		//self_Eye.transform.Rotate()
		//transform.Rotate (0, self_Eye.transform.rotation.y * 10f, 0);
		//self_Eye.transform.Rotate (0, -self_Eye.transform.rotation.y * 10f, 0);

		//player movement
		if (vr_Input_Script.touch_Status == touch_Stages.swipe_Up) {
			temp_Speed = walk_Speed;

			if (vr_Input_Script.swipe_Status == swipe_Speed.medium) {
				temp_Speed *= 2;
				temp_Text.text = "up medium";
			} else if (vr_Input_Script.swipe_Status == swipe_Speed.fast) {
				temp_Speed *= 3;
				temp_Text.text = "up fast";
			}
		} else if (vr_Input_Script.touch_Status == touch_Stages.swipe_Down) {
			temp_Speed = -walk_Speed;
		}


		transform.Translate (0, 0, temp_Speed * Time.deltaTime);
		temp_Speed = 0;

		ChangeAnimation ();

		/*if (Input.GetKey (KeyCode.W)) {

		}else if (Input.GetKey (KeyCode.S)) {
			transform.Translate (0, 0, -walk_Speed * Time.deltaTime);
		}if (Input.GetKey (KeyCode.A)) {
			transform.Rotate (0, -turn_Speed * Time.deltaTime, 0);
		}else if (Input.GetKey (KeyCode.D)) {
			transform.Rotate (0, turn_Speed * Time.deltaTime, 0);
		}*/
	}


	void ChangeAnimation(){

		if (vr_Input_Script.swipe_Status == swipe_Speed.none) {
			player_Anime.SetTrigger ("Idle");
		} else if (vr_Input_Script.swipe_Status == swipe_Speed.slow) {
			player_Anime.SetTrigger ("Walk");
		}else if (vr_Input_Script.swipe_Status == swipe_Speed.medium || vr_Input_Script.swipe_Status == swipe_Speed.fast) {
			player_Anime.SetTrigger ("Run");
		}
	}
		
	/// <summary>
	/// change the health of the player
	/// </summary>
	/// <param name="count">positive value will increase health.</param>
	public void HealthCount(int count){
		
		player_Health += count;
		//Debug.Log ("Health = " + player_Health);

		//player is dead
		if (player_Health <= 0) {
			player_Anime.SetTrigger ("Die");
			Invoke ("PauseGame", 1f);
		}
	}

	/// <summary>
	/// Pauses the game.
	/// player is dead
	/// </summary>
	void PauseGame(){

		Time.timeScale = 0;
	}

	/// <summary>
	/// change the Ammo of the player
	/// </summary>
	/// <param name="count">positive value will increase Ammo.</param>
	public void AmmoCount(int count){

		player_Ammo += count;
		//Debug.Log ("Ammo = " + player_Ammo);
	}
		
	/// <summary>
	/// Raycasting at the center of the camera to check for enemies
	/// </summary>
	void Shoot(){

		//if player has no ammo then don't shoot
		if (player_Ammo <= 0) {
			//Debug.Log ("No ammo");
			return;
		}

		RaycastHit hit;
		hit = vr_Input_Script.CastingTheRay ();

		if (hit.collider == null)
			return;
		
		if (hit.collider.CompareTag ("Enemy") || hit.collider.CompareTag ("Bug")) { 
			//an enemy has been hit call its TakeDamage function
			//if its a bug then true otherwise false will pass on
			player_Ammo--;
			hit.collider.GetComponent<EnemyCommonProperties> ().CharacterDie (hit.collider.CompareTag ("Bug"), player_HP);
			//Debug.Log ("ammo count " + player_Ammo);
		}
		
	}

	//----------------------------------------------------------------------

	#endregion
}
